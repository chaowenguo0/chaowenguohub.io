response = []

import galgebra.ga
a,b,c,d = galgebra.ga.Ga('a,b,c,d').mv()
response += (_.obj for _ in (a|(b*c), a|(b^c), a|(b^c^d), ((a|(b^c))+(c|(a^b))+(b|(c^a))).simplify(), a*(b^c)-b*(a^c)+c*(a^b), a*(b^c^d)-b*(a^c^d)+c*(a^b^d)-d*(a^b^c), (a^b)|(c^d), ((a^b)|c)|d, (a^b)>>(c^d),#commutator
                             (a^b)<<(c^d)#anti-commutator
                            )
            )

import sympy
t=sympy.symbols('t',real=True)
x,y,z=sympy.symbols('x,y,z',cls=sympy.Function,real=True)
curve=galgebra.ga.Ga('\u0411',coords=(t,),X=[x(t),y(t),z(t)])
response += tuple(_.obj for _ in curve.mv()), #base of tangent space at t
response += curve.g,
response += sympy.Integral(curve.E().norm(),t), #arclength

u,v=sympy.symbols('u,v',real=True)
surface=galgebra.ga.Ga('\u0411',coords=(u,v),X=[x(u,v),y(u,v),z(u,v)])
response += tuple(_.obj for _ in surface.mv()), #bases of tangent space at (u,v)
response += surface.g,
response += sympy.Integral(surface.E().norm(),u,v),

zenith,azimuth=sympy.symbols('theta,phi',positive=True)
spherical=galgebra.ga.Ga('\u0411',coords=(zenith,azimuth),X=[sympy.sin(zenith)*sympy.cos(azimuth),sympy.sin(zenith)*sympy.sin(azimuth),sympy.cos(zenith)])
response += sympy.integrate(spherical.E().norm(),(azimuth,0,2*sympy.pi),(zenith,0,sympy.pi)),

u,v,w=sympy.symbols('u,v,w',real=True)
solid=galgebra.ga.Ga('\u0411',coords=(u,v,w),X=[x(u,v,w),y(u,v,w),z(u,v,w)])
response += tuple(_.obj for _ in solid.mv()), #bases of tangent space at (u,v,w)
response += solid.g,
response += sympy.Integral(solid.E().norm(),u,v,w),

radius,zenith,azimuth=sympy.symbols('r,theta,phi',positive=True)
sphere=galgebra.ga.Ga('\u0411',coords=(radius,zenith,azimuth),X=[radius*sympy.sin(zenith)*sympy.cos(azimuth),radius*sympy.sin(zenith)*sympy.sin(azimuth),radius*sympy.cos(zenith)])
response += sympy.integrate(sphere.E().norm(),(azimuth,0,2*sympy.pi),(zenith,0,sympy.pi),(radius,0,1)),

import xml.dom

def dot(self, printer):
    doc = xml.dom.minidom.Document()
    x = doc.createElement('mrow')
    x.appendChild(printer._print(self.args[0]))
    sdot = doc.createElement('mo')
    sdot.appendChild(doc.createTextNode('\u22C5'))
    x.appendChild(sdot)
    x.appendChild(printer._print(self.args[-1]))
    return x
galgebra.atoms.DotProductSymbol._mathml_presentation = dot

def vector(self, printer):
    doc = xml.dom.minidom.Document()
    x = doc.createElement('mover')
    x.appendChild(printer._print_Symbol(self))
    vector = doc.createElement('mo')
    vector.appendChild(doc.createTextNode('\u20D7'))
    x.appendChild(vector)    
    return x
galgebra.atoms.BasisVectorSymbol._mathml_presentation = vector

def blade(self, printer):
    doc = xml.dom.minidom.Document()
    x = doc.createElement('mrow')
    for _ in self.args:
        x.appendChild(printer._print(_))
        wedge = doc.createElement('mo')
        wedge.appendChild(doc.createTextNode('\u2227'))
        x.appendChild(wedge)
    x.removeChild(x.lastChild)
    return x
galgebra.atoms.BasisBladeSymbol._mathml_presentation = blade

response = [sympy.mathml(_, printer='presentation') for _ in response]

import json, pathlib
pathlib.Path(__file__).resolve().parent.joinpath('response.json').write_text(json.dumps(response))
